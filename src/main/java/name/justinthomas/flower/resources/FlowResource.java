package name.justinthomas.flower.resources;

import io.dropwizard.hibernate.UnitOfWork;
import com.codahale.metrics.annotation.Timed;
import name.justinthomas.flower.FlowerConfiguration;
import name.justinthomas.flower.core.NetFlow;
import name.justinthomas.flower.core.FrequencyManager;
import name.justinthomas.flower.core.IntervalManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@Path("/flow")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FlowResource {

    private final Logger logger = LoggerFactory.getLogger(FlowResource.class.getName());
    private final FlowerConfiguration configuration;

    @Context
    HttpServletRequest request;

    public FlowResource(FlowerConfiguration configuration) {
        this.configuration = configuration;
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("{uuid}")
    public Response create(@PathParam("uuid") String uuid, List<NetFlow> netFlows) {
        Long created = 0L;
        for(NetFlow netFlow : netFlows) {
            netFlow.setReportedBy(request.getRemoteAddr());
            created += 1;
            IntervalManager.updateIntervals(netFlow, configuration.getResolution(), uuid);
        }

        return Response.ok(created).build();
    }

    @GET
    @Path("/frequencies/{uuid}")
    public Map stats(@PathParam("uuid") String uuid) {
        return FrequencyManager.getAll(uuid);
    }

    @GET
    @Path("/dump")
    @UnitOfWork
    public Response cache() {
        String messages = IntervalManager.dumpCache();
        return Response.ok(messages).build();
    }
}
