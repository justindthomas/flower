package name.justinthomas.flower.resources;

import io.dropwizard.hibernate.UnitOfWork;
import com.codahale.metrics.annotation.Timed;
import name.justinthomas.flower.FlowerConfiguration;
import name.justinthomas.flower.core.ManagedNetwork;
import name.justinthomas.flower.db.ManagedNetworkDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/network")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ManagedNetworkResource {
    private final Logger logger = LoggerFactory.getLogger(ManagedNetworkResource.class.getName());
    private final FlowerConfiguration configuration;
    private final ManagedNetworkDAO networkDAO;

    @Context
    HttpServletRequest request;

    public ManagedNetworkResource(FlowerConfiguration configuration, ManagedNetworkDAO networkDAO) {
        this.configuration = configuration;
        this.networkDAO = networkDAO;
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/create")
    public Response create(ManagedNetwork network) {
        networkDAO.create(network);

        return Response.ok().build();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/all/{uuid}")
    public Response retrieveAll(@PathParam("uuid") String uuid) {
        List<ManagedNetwork> networks = networkDAO.findAll(uuid);

        return Response.ok(networks).build();
    }

}
