package name.justinthomas.flower.resources;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.UnitOfWork;
import com.codahale.metrics.annotation.Timed;
import name.justinthomas.flower.FlowerConfiguration;
import name.justinthomas.flower.core.Account;
import name.justinthomas.flower.core.PasswordChange;
import name.justinthomas.flower.core.Organization;
import name.justinthomas.flower.core.AddOrganization;
import name.justinthomas.flower.db.AccountDAO;
import name.justinthomas.flower.db.OrganizationDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lambdaworks.crypto.SCryptUtil;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountResource {
    private final Logger logger = LoggerFactory.getLogger(ManagedNetworkResource.class.getName());
    private final FlowerConfiguration configuration;
    private final AccountDAO accountDAO;
    private final OrganizationDAO organizationDAO;

    @Context
    HttpServletRequest request;

    public AccountResource(FlowerConfiguration configuration, AccountDAO accountDAO, OrganizationDAO organizationDAO) {
        this.configuration = configuration;
        this.accountDAO = accountDAO;
        this.organizationDAO = organizationDAO;
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/create")
    public Response create(Account account) {
        Response response = Response.status(401).build();
        
        Optional<Account> storedAcct = accountDAO.getUniqueByUsername(account.getUsername());

        if(!storedAcct.isPresent()) {
            account.setPassword(SCryptUtil.scrypt(account.getPassword(), 16384, 8, 1));
            
            Optional<Organization> storedOrg =
                organizationDAO.getUniqueByUuid(account.getOrganization());
        
            if(storedOrg.isPresent()) {
                accountDAO.createOrUpdate(account);
                response = Response.ok().build();
            }
        }

        return response;
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/auth")
    public Response authenticate(Account account) {
        Optional<Account> stored = accountDAO.getUniqueByUsername(account.getUsername());

        Response response = Response.status(401).build();
        
        if(stored.isPresent()) {            
            if (SCryptUtil.check(account.getPassword(), stored.get().getPassword())) {
                response = Response.ok(stored.get().getOrganization()).build();
            }
        }
        
        return response;
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/password")
    public Response password(PasswordChange pc) {
        Optional<Account> stored = accountDAO.getUniqueByUsername(pc.getUsername());

        Response response = Response.status(401).build();
        
        if(stored.isPresent()) {            
            if (SCryptUtil.check(pc.getOldPassword(), stored.get().getPassword())) {
                Account storedAccount = stored.get();
                storedAccount.setPassword(SCryptUtil.scrypt(pc.getNewPassword(), 16384, 8, 1));
                
                accountDAO.createOrUpdate(storedAccount);
                
                response = Response.ok().build();
            }
        }
        
        return response;
    }

    @DELETE
    @Timed
    @UnitOfWork
    public Response delete(Account account) {
        Optional<Account> stored = accountDAO.getUniqueByUsername(account.getUsername());

        Response response = Response.status(401).build();
        
        if(stored.isPresent() &&
           stored.get().getOrganization().equals(account.getOrganization())) {
            accountDAO.delete(stored.get());
            response = Response.ok().build();
        }
        
        return response;
    }
    
    @GET
    @Timed
    @UnitOfWork
    @Path("/all")
    public Response retrieveAll() {
        List<Account> accounts = accountDAO.findAll();

        return Response.ok(accounts).build();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{uuid}")
    public Response create(@PathParam("uuid") String uuid) {
        List<Account> accounts = accountDAO.findByOrganization(uuid);

        return Response.ok(accounts).build();
    }
}
