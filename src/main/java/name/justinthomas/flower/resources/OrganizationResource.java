package name.justinthomas.flower.resources;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.UnitOfWork;
import com.codahale.metrics.annotation.Timed;
import name.justinthomas.flower.FlowerConfiguration;
import name.justinthomas.flower.core.Organization;
import name.justinthomas.flower.db.OrganizationDAO;
import name.justinthomas.flower.core.Account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;
import java.util.List;

@Path("/organization")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OrganizationResource {
    private final Logger logger = LoggerFactory.getLogger(OrganizationResource.class.getName());
    private final FlowerConfiguration configuration;
    private final OrganizationDAO organizationDAO;

    @Context
    HttpServletRequest request;

    public OrganizationResource(FlowerConfiguration configuration, OrganizationDAO organizationDAO) {
        this.configuration = configuration;
        this.organizationDAO = organizationDAO;
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/create")
    public Response create(Organization organization) {
        UUID uuid = UUID.randomUUID();
        organization.setUuid(uuid.toString());

        organizationDAO.createOrUpdate(organization);

        return Response.ok(uuid.toString()).build();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/all")
    public Response retrieveAll() {
        List<Organization> organizations = organizationDAO.findAll();

        return Response.ok(organizations).build();
    }

}
