package name.justinthomas.flower;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.db.DataSourceFactory;
import org.hibernate.validator.constraints.*;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

public class FlowerConfiguration extends Configuration {
    @Valid
    @NotNull
    private Long resolution;

    @JsonProperty
    public Long getResolution() {
        return resolution;
    }

    @JsonProperty
    public void setResolution(Long resolution) {
        this.resolution = resolution;
    }
    
    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    public void setDatabase(DataSourceFactory database) {
        this.database = database;
    }
}
