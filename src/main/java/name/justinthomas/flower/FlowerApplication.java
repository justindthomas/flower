package name.justinthomas.flower;

import com.google.common.collect.ImmutableList;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.SessionFactoryFactory;
import name.justinthomas.flower.core.*;
import name.justinthomas.flower.db.IntervalDAO;
import name.justinthomas.flower.db.ManagedNetworkDAO;
import name.justinthomas.flower.db.AccountDAO;
import name.justinthomas.flower.db.OrganizationDAO;
import name.justinthomas.flower.resources.FlowResource;
import name.justinthomas.flower.resources.ManagedNetworkResource;
import name.justinthomas.flower.resources.AccountResource;
import name.justinthomas.flower.resources.OrganizationResource;
import net.sf.ehcache.*;
import net.sf.ehcache.config.CacheConfiguration;

public class FlowerApplication extends Application<FlowerConfiguration> {
    private final static int MAX_CACHE_ELEMENTS = 0;
    private final static long DISK_EXPIRY_THREAD_INTERVAL_SECS = 120;
    private final static long CACHE_TTL_SECONDS = 120;
    private final static long CACHE_IDLE_TIME_SECONDS = 15;

    public static void main(final String[] args) throws Exception {
        new FlowerApplication().run(args);
    }

    private final HibernateBundle<FlowerConfiguration> hibernate = new HibernateBundle<FlowerConfiguration>(ImmutableList.of(NetFlow.class, ManagedNetwork.class, Account.class, Interval.class, Organization.class), new SessionFactoryFactory()) {
        @Override
        public DataSourceFactory getDataSourceFactory(FlowerConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public void initialize(final Bootstrap<FlowerConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(final FlowerConfiguration configuration,
                    final Environment environment) {
        final IntervalDAO intervals = new IntervalDAO(hibernate.getSessionFactory());
        final ManagedNetworkDAO networks = new ManagedNetworkDAO(hibernate.getSessionFactory());
        final AccountDAO accounts = new AccountDAO(hibernate.getSessionFactory());
        final OrganizationDAO organizations = new OrganizationDAO(hibernate.getSessionFactory());
        
        CacheManager singletonManager = CacheManager.create();

        CacheConfiguration intervalConfig = new CacheConfiguration("intervalCache",
                                                                   MAX_CACHE_ELEMENTS);
        intervalConfig.setEternal(false);
        intervalConfig.setTimeToLiveSeconds(CACHE_TTL_SECONDS);
        intervalConfig.setTimeToIdleSeconds(CACHE_IDLE_TIME_SECONDS);
        
        Cache intervalCache = new Cache(intervalConfig);
        intervalCache.getCacheEventNotificationService()
            .registerListener(new IntervalCacheEventListener(intervals));
        singletonManager.addCache(intervalCache);

        CacheConfiguration dirtyConfig = new CacheConfiguration("dirtyCacheKeys",
                                                                MAX_CACHE_ELEMENTS);
        dirtyConfig.setEternal(true);
        dirtyConfig.setOverflowToDisk(false);
        dirtyConfig.setTimeToLiveSeconds(0);
        dirtyConfig.setTimeToIdleSeconds(0);
        dirtyConfig.setDiskExpiryThreadIntervalSeconds(DISK_EXPIRY_THREAD_INTERVAL_SECS);

        Cache dirtyCacheKeys = new Cache(dirtyConfig);        
        singletonManager.addCache(dirtyCacheKeys);

        environment.jersey().register(new FlowResource(configuration));
        environment.jersey().register(new ManagedNetworkResource(configuration, networks));
        environment.jersey().register(new AccountResource(configuration,
                                                          accounts,
                                                          organizations));
        environment.jersey().register(new OrganizationResource(configuration, organizations));
    }
}
