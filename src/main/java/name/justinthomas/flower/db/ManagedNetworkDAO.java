package name.justinthomas.flower.db;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.AbstractDAO;
import name.justinthomas.flower.core.ManagedNetwork;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.List;

public class ManagedNetworkDAO extends AbstractDAO<ManagedNetwork> {
    public ManagedNetworkDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<ManagedNetwork> findById(Long id) {
        return Optional.fromNullable(get(id));
    }

    public Optional<ManagedNetwork> getUniqueByAddress(String uuid, String address) {
        Query query = namedQuery("name.justinthomas.flower.core.ManagedNetwork.find")
            .setString("uuid", uuid)
            .setString("address", address);
        return Optional.fromNullable(uniqueResult(query));
    }

    public final long create(ManagedNetwork managedNetwork) {
        return persist(managedNetwork).getId();
    }

    public final List<ManagedNetwork> findAll(String uuid) {
        return list(namedQuery("name.justinthomas.flower.core.ManagedNetwork.findAll")
                    .setString("uuid", uuid));
    }
}
