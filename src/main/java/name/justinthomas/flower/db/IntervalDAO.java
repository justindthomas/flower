package name.justinthomas.flower.db;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.AbstractDAO;
import name.justinthomas.flower.core.Interval;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.List;

public class IntervalDAO extends AbstractDAO<Interval> {
    public IntervalDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Interval> findById(Long id) {
        return Optional.fromNullable(get(id));
    }

    public Optional<Interval> findByInterval(Long interval, Long resolution) {
        Query query = namedQuery("name.justinthomas.flower.core.Interval.findByInterval");
        query.setLong("interval", interval);
        query.setLong("resolution", resolution);

        return Optional.fromNullable(uniqueResult(query));
    }

    public long create(Interval interval) {
        return persist(interval).getId();
    }

    public List<Interval> findAll() {
        return list(namedQuery("name.justinthomas.flower.core.Interval.findAll"));
    }
}
