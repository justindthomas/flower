package name.justinthomas.flower.db;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.AbstractDAO;
import name.justinthomas.flower.core.NetFlow;
import org.hibernate.SessionFactory;

import java.util.List;

public class FlowDAO extends AbstractDAO<NetFlow> {
    public FlowDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<NetFlow> findById(Long id) {
        return Optional.fromNullable(get(id));
    }

    public long create(NetFlow netFlow) {
        return persist(netFlow).getId();
    }

    public List<NetFlow> findAll() {
        return list(namedQuery("name.justinthomas.flower.core.NetFlow.findAll"));
    }
}

