package name.justinthomas.flower.db;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.AbstractDAO;
import name.justinthomas.flower.core.Account;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.List;

public class AccountDAO extends AbstractDAO<Account> {
    public AccountDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Account> findById(Long id) {
        return Optional.fromNullable(get(id));
    }

    public Optional<Account> getUniqueByUsername(String username) {
        Query query = namedQuery("name.justinthomas.flower.core.Account.findByUsername")
            .setString("username", username);

        return Optional.fromNullable(uniqueResult(query));
    }
    
    public final long createOrUpdate(Account account) {
        return persist(account).getId();
    }

    public final void delete(Account account) {
        currentSession().delete(account);
    }

    public final List<Account> findAll() {
        return list(namedQuery("name.justinthomas.flower.core.Account.findAll"));
    }

    public final List<Account> findByOrganization(String org) {
        Query query = namedQuery("name.justinthomas.flower.core.Account.findByOrganization")
            .setString("organization", org);

        return list(query);
    }
}
