package name.justinthomas.flower.db;

import com.google.common.base.Optional;
import io.dropwizard.hibernate.AbstractDAO;
import name.justinthomas.flower.core.Organization;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

import java.util.List;

public class OrganizationDAO extends AbstractDAO<Organization> {
    public OrganizationDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Organization> findById(Long id) {
        return Optional.fromNullable(get(id));
    }

    public Optional<Organization> getUniqueByUuid(String uuid) {
        Query query = namedQuery("name.justinthomas.flower.core.Organization.findByUuid")
            .setString("uuid", uuid);

        return Optional.fromNullable(uniqueResult(query));
    }

    public Optional<Organization> getUniqueByName(String name) {
        Query query = namedQuery("name.justinthomas.flower.core.Organization.findByName")
            .setString("name", name);

        return Optional.fromNullable(uniqueResult(query));
    }
    
    public final long createOrUpdate(Organization organization) {
        return persist(organization).getId();
    }

    public final List<Organization> findAll() {
        return list(namedQuery("name.justinthomas.flower.core.Organization.findAll"));
    }
}
