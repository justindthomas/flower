package name.justinthomas.flower.core;

import java.io.Serializable;

public class AnomalyEvent implements Serializable {
    public String source;
    public String destination;
    public Anomaly anomaly;
    public Integer basis;

    public static enum Anomaly {

        EWMA_INCREASE,
        EWMA_DECREASE,
        NEW_MAXIMUM,
        NEW_MINIMUM,
        NEW_FLOW
    }

    public AnomalyEvent() {

    }

    public AnomalyEvent(String source, String destination, Anomaly anomaly, Integer basis) {
        this.source = source;
        this.destination = destination;
        this.anomaly = anomaly;
        this.basis = basis;
    }
}
