package name.justinthomas.flower.core;

import javax.persistence.*;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "name.justinthomas.flower.core.ManagedNetwork.findAll",
                query = "SELECT n FROM ManagedNetwork n WHERE uuid = :uuid"
        ),
        @NamedQuery(
                name = "name.justinthomas.flower.core.ManagedNetwork.find",
                query = "SELECT n FROM ManagedNetwork n WHERE uuid = :uuid AND address = :address"
        )
})
public class ManagedNetwork implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String uuid;
    private String address;
    private String description;

    public ManagedNetwork() {

    }

    public ManagedNetwork(String uuid,
                          String address,
                          String description) throws UnknownHostException {
        this.uuid = uuid;
        InetAddress.getByName(address.split("/")[0]);
        this.address = address;
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
