package name.justinthomas.flower.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConsolidatedFlow {
    private Date startTimeStamp;
    private Date lastTimeStamp;

    private String reportedBy;
    private String source;
    private String destination;
    private List<Integer> sourcePorts = new ArrayList();
    private Integer destinationPort;
    private Integer protocol;
    private Long requestByteSize;
    private Long responseByteSize;
    private Integer requestPacketCount;
    private Integer responsePacketCount;
    private Integer flags;

    public void addFlow(NetFlow netFlow) {

    }

    public Date getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(Date startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public void addStartTimeStamp(Date startTimeStamp) {
        this.startTimeStamp = (startTimeStamp.before(this.startTimeStamp)) ? startTimeStamp : this.startTimeStamp;
    }

    public Date getLastTimeStamp() {
        return lastTimeStamp;
    }

    public void setLastTimeStamp(Date lastTimeStamp) {
        this.lastTimeStamp = lastTimeStamp;
    }

    public void addLastTimeStamp(Date lastTimeStamp) {
        this.lastTimeStamp = (lastTimeStamp.after(this.lastTimeStamp)) ? lastTimeStamp : this.lastTimeStamp;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public void setReportedBy(String reportedBy) {
        this.reportedBy = reportedBy;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Integer> getSourcePorts() {
        return sourcePorts;
    }

    public void addSourcePort(Integer port) {
        sourcePorts.add(port);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(Integer destinationPort) {
        this.destinationPort = destinationPort;
    }

    public Integer getProtocol() {
        return protocol;
    }

    public void setProtocol(Integer protocol) {
        this.protocol = protocol;
    }

    public Long getRequestByteSize() {
        return requestByteSize;
    }

    public void setRequestByteSize(Long requestByteSize) {
        this.requestByteSize = requestByteSize;
    }

    public Long getResponseByteSize() {
        return responseByteSize;
    }

    public void setResponseByteSize(Long responseByteSize) {
        this.responseByteSize = responseByteSize;
    }

    public Integer getRequestPacketCount() {
        return requestPacketCount;
    }

    public void setRequestPacketCount(Integer requestPacketCount) {
        this.requestPacketCount = requestPacketCount;
    }

    public Integer getResponsePacketCount() {
        return responsePacketCount;
    }

    public void setResponsePacketCount(Integer responsePacketCount) {
        this.responsePacketCount = responsePacketCount;
    }

    public Integer getFlags() {
        return flags;
    }

    public void setFlags(Integer flags) {
        this.flags = flags;
    }

    public void addFlags(Integer flags) {
        this.flags = this.flags | flags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConsolidatedFlow)) return false;

        ConsolidatedFlow that = (ConsolidatedFlow) o;

        if (!destination.equals(that.destination)) return false;
        if (destinationPort != null ? !destinationPort.equals(that.destinationPort) : that.destinationPort != null)
            return false;
        if (!protocol.equals(that.protocol)) return false;
        if (!source.equals(that.source)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + destination.hashCode();
        result = 31 * result + (destinationPort != null ? destinationPort.hashCode() : 0);
        result = 31 * result + protocol.hashCode();
        return result;
    }
}
