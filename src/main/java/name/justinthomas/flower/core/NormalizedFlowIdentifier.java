package name.justinthomas.flower.core;

import java.io.Serializable;

public class NormalizedFlowIdentifier implements Serializable {
    String source;
    String destination;

    protected NormalizedFlowIdentifier() {

    }

    public NormalizedFlowIdentifier(String source, String destination) {
        this.source = source;
        this.destination = destination;
    }

    @Override
    public String toString() {
        return(source + ":" + destination);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = hash * 23 + (source == null ? 0 : source.hashCode());
        hash = hash * 23 + (destination == null ? 0 : destination.hashCode());
        return hash;
    }
}
