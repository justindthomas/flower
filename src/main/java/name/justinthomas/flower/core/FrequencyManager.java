package name.justinthomas.flower.core;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FrequencyManager {
    private final static Logger logger = LoggerFactory
        .getLogger(FrequencyManager.class.getName());
    private static CacheManager cacheManager = CacheManager.create();

    public static void record(String uuid, NetFlow netFlow) {
        if(netFlow.getProtocol() != 6 && netFlow.getProtocol() != 17) {
            return;
        }

        if (!cacheManager.cacheExists(uuid)) {
            cacheManager.addCache(uuid);
        }
        
        Ehcache frequencies = cacheManager.getCache(uuid);

        List<String> keys = new ArrayList();

        if(netFlow.getDestinationPort() != netFlow.getSourcePort()) {
            keys.add(netFlow.getSourcePort() + "|" + netFlow.getProtocol());
        }

        keys.add(netFlow.getDestinationPort() + "|" + netFlow.getProtocol());

        for(String key : keys) {
            Long value = 0l;
            if(frequencies.isKeyInCache(key)) {
                value = frequencies.get(key) == null ? 0 :
                    (Long)frequencies.get(key).getObjectValue();
            }

            frequencies.put(new Element(key, ++value));
        }
    }

    public static Map getAll(String uuid) {
        if (!cacheManager.cacheExists(uuid)) {
            cacheManager.addCache(uuid);
        }
        
        Ehcache frequencies = cacheManager.getCache(uuid);

        Map<String, Long> freq = new HashMap();
        for(Object obj : frequencies.getKeys()) {
            if(frequencies.get(obj) != null) {
                freq.put((String) obj, (Long) frequencies.get(obj).getObjectValue());
            }
        }

        return freq;
    }

    public static enum Directionality {
        CORRECT,
        REVERSED,
        INDETERMINATE
    }

    public static Directionality analyze(String uuid,
                                         Integer protocol,
                                         Integer source,
                                         Integer destination) {
        if (!cacheManager.cacheExists(uuid)) {
            cacheManager.addCache(uuid);
        }
        
        Ehcache frequencies = cacheManager.getCache(uuid);

        String sourceKey = source + "|" + protocol;
        String destinationKey = destination + "|" + protocol;

        if(!frequencies.isKeyInCache(sourceKey) || !frequencies.isKeyInCache(destinationKey)) {
            return Directionality.INDETERMINATE;
        }

        Long sourceFrequency = (Long)frequencies.get(sourceKey).getObjectValue();
        Long destinationFrequency = (Long)frequencies.get(destinationKey).getObjectValue();

        return (sourceFrequency > destinationFrequency) ?
            Directionality.REVERSED : Directionality.CORRECT;
    }
}
