package name.justinthomas.flower.core;

import name.justinthomas.flower.db.IntervalDAO;
import net.sf.ehcache.*;
import net.sf.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntervalCacheEventListener implements CacheEventListener {
    private final static Logger logger = LoggerFactory.getLogger(IntervalCacheEventListener.class.getName());
    private static CacheManager singletonManager = CacheManager.create();

    private IntervalDAO intervals;

    public IntervalCacheEventListener(IntervalDAO intervals) {
        logger.info("+++++IntervalCacheEventListener started.");
        this.intervals = intervals;
    }

    public void notifyElementRemoved(Ehcache ehch, Element elmnt) throws CacheException {
        Cache dirtyCacheKeys = singletonManager.getCache("dirtyCacheKeys");
        dirtyCacheKeys.remove(elmnt.getObjectKey());
    }

    public void notifyElementPut(Ehcache ehch, Element elmnt) throws CacheException {
        Cache dirtyCacheKeys = singletonManager.getCache("dirtyCacheKeys");
        IntervalKey rowKey = (IntervalKey) elmnt.getObjectKey();
        Element elem = new Element(rowKey, new Boolean(true));
        dirtyCacheKeys.put(elem);
    }

    public void notifyElementUpdated(Ehcache ehch, Element elmnt) throws CacheException {
        // nop
    }

    public void notifyElementExpired(Ehcache ehch, Element elmnt) {
        Cache dirtyCacheKeys = singletonManager.getCache("dirtyCacheKeys");
        // Flush the element to your persistent store
        Long id = IntervalManager.persistInterval((Interval) elmnt.getObjectValue(), intervals);
        RabbitManager.sendInterval((Interval) elmnt.getObjectValue());

        dirtyCacheKeys.remove(elmnt.getObjectKey());
    }

    public void notifyElementEvicted(Ehcache ehch, Element elmnt) {
        logger.info("+++++Sending evicted Interval to RabbitMQ.");
        Cache dirtyCacheKeys = singletonManager.getCache("dirtyCacheKeys");
        // Flush the element to your persistent store
        RabbitManager.sendInterval((Interval) elmnt.getObjectValue());
        IntervalManager.persistInterval((Interval) elmnt.getObjectValue(), intervals);

        dirtyCacheKeys.remove(elmnt.getObjectKey());
    }

    public void notifyRemoveAll(Ehcache ehch) {
        Cache dirtyCacheKeys = singletonManager.getCache("dirtyCacheKeys");
        dirtyCacheKeys.removeAll();
    }

    public void dispose() {
        // nop
    }

    public Object clone() {
        return null;
    }
}
