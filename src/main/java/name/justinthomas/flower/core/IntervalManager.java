package name.justinthomas.flower.core;

import com.google.common.base.Optional;
import name.justinthomas.flower.db.IntervalDAO;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;

public class IntervalManager {
    private final static Logger logger = LoggerFactory.getLogger(IntervalManager.class.getName());
    private final static CacheManager singletonManager = CacheManager.create();

    public static String logInterval(IntervalKey key) {
        Cache intervalCache = singletonManager.getCache("intervalCache");

        if(intervalCache.isKeyInCache(key)) {
            Element element;
            if((element = intervalCache.get(key)) == null) {
                return "";
            }

            Optional<Interval> interval = Optional.fromNullable((Interval) element.getObjectValue());

            if (interval.isPresent()) {
                HashMap<NormalizedFlowIdentifier, NormalizedFlow> flows = interval.get().getFlows();
                StringBuilder messages = new StringBuilder();
                for (NormalizedFlow flow : flows.values()) {
                    HashMap<NormalizedFlowDetail, Double> count = flow.getCount();

                    for (NormalizedFlowDetail detail : count.keySet()) {
                        messages.append(key.getInterval() + " | " +
                                        flow.getSource() + " -> " +
                                        flow.getDestination() + " | " +
                                        detail.getSource() + " | " +
                                        detail.getDestination() + " | " +
                                        detail.getProtocol() + " | " +
                                        detail.getType() + " | " +
                                        detail.getVersion() + " | " +
                                        count.get(detail) + "\n");
                    }

                }
                return messages.toString();
            }
        }

        return "";
    }

    public static HashMap<IntervalKey, Interval> flowToIntervals(NetFlow netFlow,
                                                                 Long resolution,
                                                                 String uuid) {
        //logger.info("analyzing: " + netFlow.toString());
        if(netFlow.getByteSize() == 0) {
            logger.error("netFlow has 0 byte size: " + netFlow.toString());
        }

        if(netFlow.getPacketCount() == 0) {
            logger.error("netFlow has 0 packet count: " + netFlow.toString());
        }

        FrequencyManager.record(uuid, netFlow);
        HashMap<IntervalKey, Interval> normalized = new HashMap<IntervalKey, Interval>();

        Long start = (netFlow.getStartTimeStamp().getTime() / resolution);
        Long end = (netFlow.getLastTimeStamp().getTime() / resolution);
        Long spread = end - start + 1;

        for (long interval = start; interval <= end; interval++) {
            Interval sInterval = new Interval();
            sInterval.setResolution(resolution);
            sInterval.setInterval(interval);
            sInterval.setUuid(uuid);
            normalized.put(new IntervalKey(uuid, interval, resolution),
                           sInterval.addFlow(new NormalizedFlow(spread, netFlow)));
        }

        return normalized;
    }

    public static long persistInterval(Interval interval, IntervalDAO intervalDAO) {
        return intervalDAO.create(interval);
    }

    private static void updateInterval(Interval interval) {
        IntervalKey key = new IntervalKey(interval.getUuid(),
                                          interval.getInterval(),
                                          interval.getResolution());
        Cache intervalCache = singletonManager.getCache("intervalCache");

        Element stored = getInterval(key);
        if (stored != null) {
            ((Interval) stored.getObjectValue()).addInterval(interval);
            intervalCache.put(stored);
        } else {
            intervalCache.put(new Element(key, interval));
        }
    }

    public static Boolean updateIntervals(NetFlow netFlow, Long resolution, String uuid) {
        HashMap<IntervalKey, Interval> intervals = IntervalManager.flowToIntervals(netFlow,
                                                                                   resolution,
                                                                                   uuid);

        for (IntervalKey key : intervals.keySet()) {
            updateInterval(intervals.get(key));
        }

        return true;
    }

    public static Element getInterval(IntervalKey key) {
        Cache dirtyCacheKeys = singletonManager.getCache("dirtyCacheKeys");
        Cache intervalCache = singletonManager.getCache("intervalCache");

        while (true) {
            Element cacheElement = intervalCache.get(key);
            if (cacheElement != null) {
                return cacheElement;
            }

            if (!dirtyCacheKeys.isKeyInCache(key)) {
                break;
            }

            // This means that element is not in cache but is getting flushed to persistent store now.
            Thread.yield();
        }

        return null;
    }

    public static String dumpCache() {
        Cache intervalCache = singletonManager.getCache("intervalCache");
        StringBuilder builder = new StringBuilder();
        for (Object key : intervalCache.getKeys()) {
            builder.append(logInterval((IntervalKey) key));
        }
        return builder.toString();
    }
}
