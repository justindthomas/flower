package name.justinthomas.flower.core;

import java.io.Serializable;

public class AddOrganization implements Serializable {

    private String username;
    private String organizationName;

    public AddOrganization() {

    }

    public AddOrganization(String username, String organizationName) {
        this.username = username;
        this.organizationName = organizationName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
