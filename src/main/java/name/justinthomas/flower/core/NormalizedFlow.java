package name.justinthomas.flower.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class NormalizedFlow implements Serializable {
    private final static Logger logger = LoggerFactory.getLogger(NormalizedFlow.class.getName());

    String source;
    String destination;
    HashMap<NormalizedFlowDetail, Double> count = new HashMap();

    public NormalizedFlowIdentifier id() {
        return new NormalizedFlowIdentifier(source, destination);
    }

    public NormalizedFlow() {
    }

    public NormalizedFlow(Long spread, NetFlow netFlow) {
        if(spread == 0) spread = 1l;

        try {
            source = netFlow.getSource();
            destination = netFlow.getDestination();


            NormalizedFlowDetail.Version version = null;
            if(InetAddress.getByName(netFlow.getSource()) instanceof Inet4Address) {
                version = NormalizedFlowDetail.Version.IPV4;
            } else if(InetAddress.getByName(netFlow.getSource()) instanceof Inet6Address) {
                version = NormalizedFlowDetail.Version.IPV6;
            }

            NormalizedFlowDetail bytes = new NormalizedFlowDetail(NormalizedFlowDetail.Count.BYTE, version, netFlow.getProtocol(), netFlow.getSourcePort(), netFlow.getDestinationPort());
            NormalizedFlowDetail packets = new NormalizedFlowDetail(NormalizedFlowDetail.Count.PACKET, version, netFlow.getProtocol(), netFlow.getSourcePort(), netFlow.getDestinationPort());

            if (count.containsKey(bytes)) {
                count.put(bytes, (netFlow.getByteSize().doubleValue() / spread) + count.get(bytes));
            } else {
                count.put(bytes, (netFlow.getByteSize().doubleValue() / spread));
            }

            if (count.containsKey(packets)) {
                count.put(packets, (netFlow.getPacketCount().doubleValue() / spread) + count.get(packets));
            } else {
                count.put(packets, (netFlow.getPacketCount().doubleValue() / spread));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public NormalizedFlow addFlow(NormalizedFlow flow) {
        if(this.destination.equals(flow.destination) && this.source.equals(flow.source)) {
            for(Map.Entry<NormalizedFlowDetail, Double> entry : flow.count.entrySet()) {
                if(this.count.containsKey(entry.getKey())) {
                    if(entry.getValue() == 0 || this.count.get(entry.getKey()) == 0) {
                        logger.error("NormalizedFlow contains 0 value: " + entry.getKey().toString());
                    }
                    this.count.put(entry.getKey(), entry.getValue() + this.count.get(entry.getKey()));
                } else {
                    this.count.put(entry.getKey(), entry.getValue());
                }
            }
            return this;
        }

        logger.error("attempted to add flow to a non-matching flow: " + flow.toString());
        return null;
    }

    public String getDestination() {
        return destination;
    }

    public String getSource() {
        return source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public HashMap<NormalizedFlowDetail, Double> getCount() {
        return count;
    }

    public void setCount(HashMap<NormalizedFlowDetail, Double> count) {
        this.count = count;
    }
}
