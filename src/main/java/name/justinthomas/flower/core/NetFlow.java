package name.justinthomas.flower.core;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author justin
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = "name.justinthomas.flower.core.NetFlow.findAll",
                query = "SELECT f FROM NetFlow f"
        )
})
public class NetFlow {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date startTimeStamp;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastTimeStamp;

    private Long startTimeStampMs;
    private Long lastTimeStampMs;
    private String reportedBy;
    private String source;
    private String destination;
    private Integer sourcePort;
    private Integer destinationPort;
    private Integer protocol;
    private Integer packetCount;
    private Integer flags;
    private Long byteSize;
    private String accountUuid;
    
    public NetFlow() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountUuid() {
        return this.accountUuid;
    }

    public void setAccountUuid(String uuid) {
        this.accountUuid = accountUuid;
    }

    public Long getLastTimeStampMs() {
        return lastTimeStampMs;
    }

    public void setLastTimeStampMs(Long lastTimeStampMs) {
        this.lastTimeStampMs = lastTimeStampMs;
        this.lastTimeStamp = new Date(lastTimeStampMs);
    }

    public Long getStartTimeStampMs() {
        return startTimeStampMs;
    }

    public void setStartTimeStampMs(Long startTimeStampMs) {
        this.startTimeStampMs = startTimeStampMs;
        this.startTimeStamp = new Date(startTimeStampMs);
    }

    public Date getStartTimeStamp() {
        return new Date(this.startTimeStampMs);
    }

    public Date getLastTimeStamp() {
        return new Date(this.lastTimeStampMs);
    }
    
    public void setLastTimeStampSecs(Long secs) {
        this.lastTimeStamp = new Date(secs * 1000);
    }

    public void setStartTimeStampSecs(Long secs) {
        this.startTimeStamp = new Date(secs * 1000);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(Integer destinationPort) {
        this.destinationPort = destinationPort;
    }

    public Integer getFlags() {
        return flags;
    }

    public void setFlags(Integer flags) {
        this.flags = flags;
    }

    public void setLastTimeStamp(Date lastTimeStamp) {
        this.lastTimeStamp = lastTimeStamp;
    }

    public Integer getPacketCount() {
        return packetCount;
    }

    public void setPacketCount(Integer packetCount) {
        this.packetCount = packetCount;
    }

    public Integer getProtocol() {
        return protocol;
    }

    public void setProtocol(Integer protocol) {
        this.protocol = protocol;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public void setReportedBy(String reportedBy) {
        this.reportedBy = reportedBy;
    }

    public Long getByteSize() {
        return byteSize;
    }

    public void setByteSize(Long byteSize) {
        this.byteSize = byteSize;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getSourcePort() {
        return sourcePort;
    }

    public void setSourcePort(Integer sourcePort) {
        this.sourcePort = sourcePort;
    }

    public void setStartTimeStamp(Date startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.getStartTimeStampMs().toString());
        builder.append(" -- ");
        builder.append(this.getSource());
        builder.append(":");
        builder.append(this.getSourcePort());
        builder.append(" -> ");
        builder.append(this.getDestination());
        builder.append(":");
        builder.append(this.getDestinationPort());

        return builder.toString();
    }
}
