package name.justinthomas.flower.core;

import java.io.Serializable;

public class NormalizedFlowDetail implements Serializable {
    public static enum Count {
        PACKET, BYTE;
    }

    public static enum Version {
        IPV6, IPV4;
    }

    protected Count type;
    protected Version version;
    protected Integer protocol;
    protected Integer source;
    protected Integer destination;

    protected NormalizedFlowDetail() { }
    public NormalizedFlowDetail(Count type, Version version, Integer protocol, Integer source, Integer destination) {
        this.type = type;
        this.version = version;
        this.protocol = protocol;
        this.source = source;
        this.destination = destination;
    }

    public Integer getDestination() {
        return destination;
    }

    public void setDestination(Integer destination) {
        this.destination = destination;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Integer getProtocol() {
        return protocol;
    }

    public void setProtocol(Integer protocol) {
        this.protocol = protocol;
    }

    public Count getType() {
        return type;
    }

    public void setType(Count type) {
        this.type = type;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return type + ":" + version + ":" + protocol + ":" + source + ":" + destination;
    }
}
