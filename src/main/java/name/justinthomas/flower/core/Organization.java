package name.justinthomas.flower.core;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import name.justinthomas.flower.core.Account;

@Entity
@NamedQueries({
        @NamedQuery(
                    name = "name.justinthomas.flower.core.Organization.findAll",
                    query = "SELECT n FROM Organization n"
                    ),
        @NamedQuery(
                    name = "name.justinthomas.flower.core.Organization.findByName",
                    query = "SELECT n FROM Organization n WHERE name = :name"
                    ),
        @NamedQuery(
                    name = "name.justinthomas.flower.core.Organization.findByUuid",
                    query = "SELECT n FROM Organization n WHERE uuid = :uuid"
                    )   
})

public class Organization implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String uuid;

    @NotEmpty
    private String name;

    public Organization() {

    }

    public Organization(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
