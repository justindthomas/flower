package name.justinthomas.flower.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "name.justinthomas.flower.core.Interval.findByInterval",
                query = "SELECT i FROM Interval i WHERE interval = :interval AND resolution = :resolution"
        )
})
public class Interval implements Serializable {
    @Transient
    private static final Logger logger = LoggerFactory.getLogger(Interval.class.getName());

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long interval;
    private Long resolution;
    private String uuid;

    @Transient
    private HashMap<NormalizedFlowIdentifier, NormalizedFlow> flows;
    private ArrayList<AnomalyEvent> anomalies;


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Interval addAnomaly(NormalizedFlowIdentifier id, AnomalyEvent.Anomaly anomaly, Integer basis) {
        this.getAnomalies().add(new AnomalyEvent(id.getSource(), id.getDestination(), anomaly, basis));
        return this;
    }

    public Interval addFlow(NormalizedFlow flow) {
        if (this.getFlows().containsKey(flow.id())) {
            this.getFlows().put(flow.id(), flow.addFlow(flow));
        } else {
            this.getFlows().put(flow.id(), flow);
        }

        return this;
    }

    public Interval addInterval(Interval interval) {
        for (Map.Entry<NormalizedFlowIdentifier, NormalizedFlow> entry : interval.getFlows().entrySet()) {
            if (this.getFlows().containsKey(entry.getKey())) {
                this.getFlows().put(entry.getKey(), this.getFlows().get(entry.getKey()).addFlow(entry.getValue()));
            } else {
                this.getFlows().put(entry.getKey(), entry.getValue());
            }
        }

        return this;
    }

    public ArrayList<AnomalyEvent> getAnomalies() {
        if(anomalies == null) {
            anomalies = new ArrayList();
        }

        return anomalies;
    }

    public void setAnomalies(ArrayList<AnomalyEvent> anomalies) {
        this.anomalies = anomalies;
    }

    public HashMap<NormalizedFlowIdentifier, NormalizedFlow> getFlows() {
        if (flows == null) {
            flows = new HashMap();
        }

        return flows;
    }

    public void setFlows(HashMap<NormalizedFlowIdentifier, NormalizedFlow> flows) {
        this.flows = flows;
    }

    public Long getResolution() {
        return resolution;
    }

    public void setResolution(Long resolution) {
        this.resolution = resolution;
    }

    public Long getInterval() {
        return interval;
    }

    public void setInterval(Long interval) {
        this.interval = interval;
    }

    @Transient
    public String toJson() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert interval to JSON");
            return null;
        }
    }
}
