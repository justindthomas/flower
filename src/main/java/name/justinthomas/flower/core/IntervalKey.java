package name.justinthomas.flower.core;

import java.io.Serializable;

public class IntervalKey implements Serializable {
    private String uuid;
    private Long interval;
    private Long resolution;

    protected IntervalKey() {
    }

    public IntervalKey(String uuid, Long interval, Long resolution) {
        this.uuid = uuid;
        this.interval = interval;
        this.resolution = resolution;
    }

    public Long getInterval() {
        return interval;
    }

    public void setInterval(Long interval) {
        this.interval = interval;
    }

    public Long getResolution() {
        return resolution;
    }

    public void setResolution(Long resolution) {
        this.resolution = resolution;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IntervalKey other = (IntervalKey) obj;
        if (this.interval != other.interval &&
            (this.interval == null || !this.interval.equals(other.interval))) {
            return false;
        }
        if (this.resolution != other.resolution &&
            (this.resolution == null || !this.resolution.equals(other.resolution))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        hash = 43 * hash + (this.interval != null ? this.interval.hashCode() : 0);
        hash = 43 * hash + (this.resolution != null ? this.resolution.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return this.uuid + ":" + this.resolution + ":" + this.interval;
    }
}
