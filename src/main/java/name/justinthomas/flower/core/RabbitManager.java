package name.justinthomas.flower.core;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class RabbitManager {
    private final static Logger logger = LoggerFactory.getLogger(RabbitManager.class.getName());
    private final static String EXCHANGE_NAME = "intervals";
    private static ConnectionFactory factory = null;
    private static Connection connection = null;
    private static Channel channel = null;
    
    public static void sendInterval(Interval interval) {
        try {
            if(factory == null) {
                factory = new ConnectionFactory();
                factory.setHost("localhost");
            }

            if (connection == null) {
                connection = factory.newConnection();
            }

            if (channel == null) {
                channel = connection.createChannel();
            }

            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
            channel.basicPublish(EXCHANGE_NAME, "", null, interval.toJson().getBytes());
        } catch (IOException e) {
            logger.error("Failed to publish interval to exchange");

            channel = null;
            connection = null;
        }
    }
}
