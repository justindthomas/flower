package name.justinthomas.flower.core;

import javax.persistence.*;
import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@NamedQueries({
        @NamedQuery(
                    name = "name.justinthomas.flower.core.Account.findAll",
                    query = "SELECT n FROM Account n"
                    ),
        @NamedQuery(
                    name = "name.justinthomas.flower.core.Account.findByUsername",
                    query = "SELECT n FROM Account n WHERE username = :username"
                    ),
        @NamedQuery(
                    name = "name.justinthomas.flower.core.Account.findByOrganization",
                    query = "SELECT n from Account n WHERE organization = :organization"
                    )
})

public class Account implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @Column(name="username",unique=true)
    private String username;

    @NotEmpty
    private String password;

    @NotEmpty
    private String organization;

    public Account() {

    }

    public Account(String username, String password, String organization) {
        this.username = username;
        this.password = password;
        this.organization = organization;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
